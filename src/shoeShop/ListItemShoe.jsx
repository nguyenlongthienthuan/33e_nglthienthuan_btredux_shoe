import React, { Component } from 'react'
import { dataShoes } from './data'
import ItemShoe from './ItemShoe'

export default class ListItemShoe extends Component {
    renderList=()=>{
        return dataShoes.map((shoe,index)=>{return  <ItemShoe key={index} id={shoe.id} shoe={shoe}></ItemShoe>})
    }
  render() {
    return (
      <div className='grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-3 container mx-auto pt-14'>
          {this.renderList()}
      </div>
    )
  }
}
