import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  REMOVE_CART,
} from "../constants/constantsShoeShop";

export const handelAddToCart = (payload) => {
  return {
    type: ADD_TO_CART,
    payload,
  };
};
export const handelChangeQuantity = (id, payload) => {
  return {
    type: CHANGE_QUANTITY,
    payload,
    id,
  };
};
export const handelRemoveCart = (payload) => {
  return {
    type: REMOVE_CART,
    payload,
  };
};
