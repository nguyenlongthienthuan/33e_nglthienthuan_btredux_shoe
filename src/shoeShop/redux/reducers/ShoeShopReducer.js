import { saveToLocalStore } from "../../saveToLocalStore";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  GET_LOCAL_STORE,
  REMOVE_CART,
  SAVE_LOCAL_STORE,
} from "../constants/constantsShoeShop";

let initialState = {
  dataBuy: [],
};
export const ShoeShopReducer = (
  state = initialState,
  { type, payload, id }
) => {
  switch (type) {
    case ADD_TO_CART:
      var index = state.dataBuy.findIndex((shoe) => {
        return shoe.id == payload.id;
      });
      return index == -1 //check da ton tai ?
        ? {
            //neu chua ton tai
            ...state,
            dataBuy: [...state.dataBuy, { ...payload, soLuong: 1 }],
          }
        : {
            //neu da ton tai
            ...state, //clone state
            dataBuy: [
              ...state.dataBuy.fill(
                //thay the phan tu
                {
                  //phan tu
                  ...payload, //clone oldobjs
                  soLuong: state.dataBuy[index].soLuong + 1, //them obj soluong
                },
                index, //tai vi tri
                index + 1
              ),
            ],
          };

    case CHANGE_QUANTITY:
      var index = state.dataBuy.findIndex((shoe) => {
        return shoe.id == id;
      });
      return {
        ...state,
        dataBuy: [
          ...state.dataBuy.fill(
            {
              ...state.dataBuy[index],
              soLuong: state.dataBuy[index].soLuong + payload,
            },
            index,
            index + 1
          ),
        ],
      };
    case REMOVE_CART:
      var index = state.dataBuy.findIndex((shoe) => {
        return shoe.id == payload.id;
      });
      state.dataBuy.splice(index, 1);
      return {
        ...state,
        dataBuy: [...state.dataBuy],
      };
    case GET_LOCAL_STORE:
      return { ...state, dataBuy: payload != null ? payload : [] };
    case SAVE_LOCAL_STORE:
      saveToLocalStore(state.dataBuy);
      return state;
    default:
      return state;
  }
};
