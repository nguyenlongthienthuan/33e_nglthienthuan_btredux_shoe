import React, { Component, PureComponent } from 'react'
import { connect } from 'react-redux'
import { handelAddToCart } from './redux/actions/actionsShoeShop'
import { SAVE_LOCAL_STORE } from './redux/constants/constantsShoeShop';

 class ItemShoe extends PureComponent {
  render() {
    console.log('itemshoe');
    let {shoe}=this.props
    return (
      <div>
        
  <div className="w-full h-full max-w-sm bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
  <a href="#">
    <img className="p-8 rounded-t-lg" src={shoe.image} alt="product image" />
  </a>
  <div className="px-5 pb-5">
    <a href="#">
      <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">{shoe.name}</h5>
    </a>
    <div className="flex items-center mt-2.5 mb-5">
      <p>{shoe.description}</p>
    </div>
    <div className="flex justify-between items-center">
      <span className="text-3xl font-bold text-gray-900 dark:text-white">{shoe.price}$</span>
      <button onClick={()=>{this.props.handelAddToCart(shoe)}} href="" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Add to cart</button>
    </div>
  </div>
</div>

      </div>
    )
  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    handelAddToCart:(payload)=>{
      return dispatch(handelAddToCart(payload));
    },
  }
}
export default connect(null,mapDispatchToProps)(ItemShoe)
