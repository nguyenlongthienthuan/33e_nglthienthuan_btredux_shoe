import React, { Component } from 'react'
import ListItemShoe from './ListItemShoe'
import ShoppingCart from './ShoppingCart'

export default class ShoeShopLayOut extends Component {
  render() {
    return (
      <div>
        <ShoppingCart></ShoppingCart>   
        <ListItemShoe></ListItemShoe>
      </div>
    )
  }
}
