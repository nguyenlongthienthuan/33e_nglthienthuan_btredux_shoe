import logo from "./logo.svg";
import "./App.css";
import ShoeShopLayOut from "./shoeShop/ShoeShopLayOut";

function App() {
  return (
    <div className="App">
      <ShoeShopLayOut></ShoeShopLayOut>
    </div>
  );
}

export default App;
